#TODO:
* reconfigure Pipeline to eat less CPU
* configure backup
* notice flaresolverr eat a lot of resources
* documentation profile for CPU
* Use the exact folder layout described by [TRaSH-Guides](https://trash-guides.info/Hardlinks/How-to-setup-for/Docker/#folder-structure)
```
mkdir -p data/{torrents,media}/{books,movies,music,tv} data/usenet/incomplete data/usenet/complete/{books,movies,music,tv}
```
   * done in longhorn branch need to migrate menfin
* Test [qBittorrent](https://github.com/qbittorrent/qBittorrent)
   * done it is better than Transmission (response during load)
* SABnzbd use emptyDir for /config/Downloads
   * tested need to migrate menfin
* __Replace RollingUpdate with Recreate__
* Test https://github.com/Flemmarr/Flemmarr
   * no more maintainer
* Test https://github.com/Prowlarr/Prowlarr/issues/1910 Tor Proxy for everyone
   * waiting for a patch: https://github.com/FlareSolverr/FlareSolverr/issues/1001
