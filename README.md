# K3S and Flux Automation

[Install k3s](https://docs.k3s.io/quick-start#install-script)
```
curl -sfL https://get.k3s.io | sh -
```

Deploy k3s-media-server
```
kubectl apply -k https://gitlab.com/k3s-media/k3s-media-server/install/?timeout=120&ref=main
```

Create a secret:
```
kubectl create secret generic k3s-media-server -n flux-system \
  --from-literal=LETS_ENCRYPT_EMAIL=<email_address> \
  --from-literal=DOMAIN=<example.com>
```


## Optional
[Install flux CLI](https://fluxcd.io/flux/installation/#install-the-flux-cli)
```
curl -s https://fluxcd.io/install.sh | sudo bash
```

# Setup Applications

See the [TRaSH-Guides](https://trash-guides.info/) for complete explanation.


Configure Step by step in this order:
- configure jellyfin: https://stream.${DOMAIN}
- configure prowlarr: https://prowlarr.${DOMAIN}
- configure jackett: https://jackett.${DOMAIN}
- configure radarr: https://radarr.${DOMAIN}
- configure sonarr: https://sonarr.${DOMAIN}
- configure jellyseer: https://search.${DOMAIN}
- configure qbittorrent: https://torrents.${DOMAIN}
- configure SABnzbd: https://sabnzbd.${DOMAIN}
- configure bazarr: https://bazarr.${DOMAIN}

Setup Jellyfin:
 - https://stream.${DOMAIN}
 - Follow Setup add /media for media folder

Setup JellySeer:
 - https://search.${DOMAIN}

Setup SABnzbd:
 - [Due to security protection for SABnzbd](https://sabnzbd.org/wiki/extra/hostname-check.html) after installation you have no access to SABnzbd, you need to setup a login and a password.
 - kubectl port-forward -n media-server deploy/sabnzbd 8080:8080
 - [Access on localhost](http://localhost:8080)
 - In General page, security section: set a login and a password in general - save the configuration
 - kubectl delete pod -n media-server -l app=sabnzbd


